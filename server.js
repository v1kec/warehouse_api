const dotenv = require("dotenv");
dotenv.config();
const express = require("express");
const mongoose = require("mongoose");
const config = require("config");
const cors = require("cors");
const items = require("./routes/api/items");
const shops = require("./routes/api/shops");
const users = require("./routes/api/users");
const auth = require("./routes/api/auth");
const tagged = require("./routes/api/tagged");

const app = express();

const { nodeEnv } = require("./config/config");
console.log("Node env is set to: " + nodeEnv);

app.use(express.json());
app.use(cors());
// Database Config
const db = config.get("mongoURI");

// Database connection
const conn = require("./index.js");
// mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false })
//     .then(() => console.log('MongoDB Connected...'))
//     .catch(err => console.log(err));

// Routes usage
app.use("/api/items/:id", items);
app.use("/api/items", items);
app.use("/api/shops", shops);
app.use("/api/shops/:id", shops);
app.use("/api/users", users);
app.use("/api/auth", auth);
app.use("/api/tagged", tagged);
app.use("/api/tagged/:id", tagged);

const port = process.env.PORT || 5000;

conn.connect().then(() => {
	app.listen(port, () => {
		console.log(`Server started on port ${port}`);
	});
});

module.exports = app;
// app.listen(port, () => console.log(`Server started on port ${port}`))
