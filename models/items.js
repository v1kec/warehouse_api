const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ItemSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    manufacturer: {
        type: String,
        required: true
    },
    quantity: { // NEW
        type: Number,
        required: true
    }, 
    isEditable: {
        type: Boolean,
        default: false
    }
})

module.exports = Item = mongoose.model('item', ItemSchema);