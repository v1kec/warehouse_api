const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TaggedSchema = new Schema({
    shop: {
        type: Schema.Types.ObjectId,
        ref: 'shop',
        unique: true
    },
    items: [{
        type: Schema.Types.ObjectId,
        ref: 'item',
    }]
})

TaggedSchema.index({shop: 1, items: 1}, {unique: true})

module.exports = Tagged = mongoose.model('tag', TaggedSchema);

