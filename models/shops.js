const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ShopSchema = new Schema({
    name: {
        type: String,
        required: true
    }
})


module.exports = Shop = mongoose.model('shop', ShopSchema);