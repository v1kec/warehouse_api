const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth')
// Auth controller
const authController = require('../../controllers/auth')

router.route('/user')
    .get(auth, authController.getUser)

router.route('/')
    .post(authController.authUser)

module.exports = router