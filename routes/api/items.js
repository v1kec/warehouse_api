const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
// Items controller
const itemsController = require("../../controllers/items");

router
	.route("/")
	.get(auth, itemsController.getItems)
	.post(auth, itemsController.addItem)
	.put(auth, itemsController.editItem);

router
	.route("/:id")
	.delete(auth, itemsController.deleteItem)
	.get(auth, itemsController.getSingleItem);

module.exports = router;
