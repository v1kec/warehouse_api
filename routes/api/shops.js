const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
// Shops controller
const shopsController = require("../../controllers/shops");

router
	.route("/")
	.get(auth, shopsController.getShops)
	.post(auth, shopsController.addShop);

router
	.route("/:id")
	.get(auth, shopsController.getSingleShop)
	.delete(auth, shopsController.deleteShop)
	.put(auth, shopsController.editShop);
module.exports = router;
