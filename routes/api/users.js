const express = require('express');
const router = express.Router();

// User Controller
const userController = require('../../controllers/users')
// Create user
router.route('/')
    .get(userController.createUser);

module.exports = router