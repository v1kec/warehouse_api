const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
// Tagged controller
const taggedController = require("../../controllers/tagged");

// GET -> api/tagged
router
	.route("/")
	.get(auth, taggedController.getTagged)
	.post(auth, taggedController.addEditTagged);

router
	.route("/:id")
	.get(taggedController.getSingleTag)
	.delete(taggedController.deleteTag);

module.exports = router;
