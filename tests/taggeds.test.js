// process.env.NODE_ENV = 'test';
let { nodeEnv } = require("../config/config");
process.env.NODE_ENV = "test";

const expect = require("chai").expect;
const should = require("chai").should;
const request = require("supertest");
const router = require("../routes/api/tagged");
const conn = require("../index.js");
const app = require("../server");
const token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlODMzOGE4NDdhZTJlMzIwMzMwYWIwMSIsImlhdCI6MTU5MTI3NTYyMSwiZXhwIjoxNTkxMjc5MjIxfQ.oGuf2e77EMsY8V9LAXvYpgkiZkbF-pyYpO_QN88sHg4";
const Tagged = require("../models/tagged");
const mongoose = require("mongoose");
const Mockgoose = require("mockgoose").Mockgoose;
const mockgoose = new Mockgoose(mongoose);

describe("Tagged API", () => {
  before((done) => {
    conn
      .connect()
      .then(() => done())
      .catch((err) => done());
  });

  describe("GET Tagged", () => {
    it("It Should GET all tagged shops with status: 200", (done) => {
      request(app)
        .get("/api/tagged")
        .set("x-auth-token", token)
        .end((err, res) => {
          expect("Content-Type", "/json/");
          expect(res.statusCode).to.equal(200);
          expect(res.body.length).to.equal(0);
          done();
        });
    });

    it("It should return status: 404 due to wrong route", (done) => {
      request(app)
        .get("/api/tagge")
        .set("x-auth-token", token)
        .end((err, res) => {
          expect(res.statusCode).to.equal(404);
          done();
        });
    });

    it("It should return status: 401 + correct error message due to missing TOKEN", (done) => {
      request(app)
        .get("/api/tagged")
        .set("x-auth-token", "")
        .end((err, res) => {
          expect(res.statusCode).to.equal(401);
          expect(res.body.msg).to.eq("No token , authorization denied");
          done();
        });
    });
  });

  afterEach(() => {
    mockgoose.helper.reset();
  });

  after((done) => {
    conn
      .close()
      .then(() => done())
      .catch((err) => done());
  });
});
