// process.env.NODE_ENV = 'test';
const dotenv = require("dotenv");
dotenv.config();
process.env.NODE_ENV = "test";
let { nodeEnv } = require("../config/config");
//
const expect = require("chai").expect;
const should = require("chai").should;
const request = require("supertest");
const { getItems } = require("../controllers/items");
const conn = require("../index.js");
const app = require("../server");
const token =
	"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlODMzOGE4NDdhZTJlMzIwMzMwYWIwMSIsImlhdCI6MTU5MTI3NTYyMSwiZXhwIjoxNTkxMjc5MjIxfQ.oGuf2e77EMsY8V9LAXvYpgkiZkbF-pyYpO_QN88sHg4";
const Item = require("../models/items");
const mongoose = require("mongoose");
const Mockgoose = require("mockgoose").Mockgoose;
const mockgoose = new Mockgoose(mongoose);

describe("Items API", () => {
	before((done) => {
		conn
			.connect()
			.then(() => done())
			.catch((err) => done());
	});

	describe("GET Routes", () => {
		it("It should GET all items (0) with TOKEN", (done) => {
			request(app)
				.get("/api/items")
				.set("x-auth-token", token)
				.end((err, res) => {
					expect("Content-Type", "/json/");
					expect(res.body).to.be.an("array");
					expect(res.body.length).to.equal(0);
					expect(200);
					done();
				});
		});

		it("It should NOT GET any items due to wrong route", (done) => {
			request(app)
				.get("/api/item")
				.set("x-auth-token", token)
				.end((err, res) => {
					// Ne e specificiran statutos vo kontrolerot
					expect(404);
					done();
				});
		});

		it("It should GET all items (1) with TOKEN", (done) => {
			let item = new Item({
				name: "Testing_Name",
				manufacturer: "Testing_Manufacturer",
				quantity: 10,
			});
			item.save((err, item) => {
				request(app)
					.get("/api/items")
					.set("x-auth-token", token)
					.end((err, res) => {
						expect("Content-Type", "/json/");
						expect(res.body).to.be.an("array");
						expect(res.body.length).to.equal(1);
						expect(res.body[0]).to.have.property("name");
						expect(res.body[0]).to.have.property("manufacturer");
						expect(res.body[0]).to.have.property("quantity");
						expect(res.body[0]).to.have.property("isEditable");
						expect(res.body[0]).to.have.property("_id");
						expect(200);
						done();
					});
			});
		});

		it("It should fail to GET all items due to missing TOKEN", (done) => {
			request(app)
				.get("/api/item")
				.set("x-auth-token", "")
				.end((err, res) => {
					// Ne e specificiran statutos vo kontrolerot
					expect(404);
					done();
				});
		});
	});

	describe("POST routes", () => {
		it("It should POST a newly created item correctly", (done) => {
			request(app)
				.post("/api/items")
				.set("x-auth-token", token)
				.send({
					name: "Testing_Name",
					manufacturer: "Testing_Manufacturer",
					quantity: 10,
				})
				.end((err, res) => {
					expect(res.body).to.have.property("name");
					expect(res.body).to.have.property("manufacturer");
					expect(res.body).to.have.property("quantity");
					expect(res.body).to.have.property("name").equal("Testing_Name");
					expect(res.body)
						.to.have.property("manufacturer")
						.equal("Testing_Manufacturer");
					expect(res.body).to.have.property("quantity").equal(10);
					expect(res.body).to.have.property("isEditable").equal(false);
					expect(res.body).to.be.a("object");
					expect(200);
					done();
				});
		});

		it("It should NOT POST a newly created item due to missing name field", (done) => {
			request(app)
				.post("/api/items")
				.set("x-auth-token", token)
				.send({
					name: "",
					manufacturer: "Testing_Manufacturer1",
					quantity: 20,
					isEditable: false,
				})
				.end((err, res) => {
					// Ne e specificiran statutos vo kontrolerot
					expect(404);
					done();
				});
		});

		it("It should NOT POST a newly created item due to missing token", (done) => {
			request(app)
				.post("/api/items")
				.set("x-auth-token", "")
				.send({
					name: "Testing_Name",
					manufacturer: "Testing_Manufacturer",
					quantity: 10,
					isEditable: false,
				})
				.end((err, res) => {
					// Ne e specificiran statutos vo kontrolerot
					expect(404);
					done();
				});
		});
	});

	describe("PUT routes", () => {
		it("It should UPDATE (PUT) selected item", (done) => {
			let item = new Item({
				name: "Testing_Name",
				manufacturer: "Testing_Manufacturer",
				quantity: 10,
			});
			item.save((err, item) => {
				const id = item._id;
				request(app)
					.put("/api/items")
					.set("x-auth-token", token)
					.send({
						name: "Updated_Name",
						manufacturer: "Testing_Manufacturer",
						id: id,
					})
					.end((err, res) => {
						expect(200);
						expect(res.body).to.be.a("object");
						expect(res.body).to.have.property("name");
						expect(res.body).to.have.property("manufacturer");
						expect(res.body).to.have.property("quantity");
						expect(res.body).to.have.property("isEditable");
						done();
					});
			});
		});
	});

	describe("DELETE routes", () => {
		// it('It should DELETE selected item', (done) => {
		//     request(app)
		//         .post('/api/items')
		//         .set('x-auth-token', token)
		//         .send({
		//             name: 'Testing_Name',
		//             manufacturer: 'Testing_Manufacturer',
		//             quantity: 10,
		//         })
		//         .then((err, res) => {
		//             const id = res._id
		//             request(app)
		//             .delete(`/api/items/${id}`)
		//             .set('x-auth-token', token)
		//             .end((err, res) => {
		//                 expect(200)
		//                 console.log(res.body)
		//                 done()
		//             })
		//         })
		// let item = new Item({ name: 'Testing_Name', manufacturer: 'Testing_Manufacturer', quantity: 10 })
		// item.save((err, item) => {
		//     const id = item._id
		//     request(app)
		//         .delete(`/api/items/${id}`)
		//         .set('x-auth-token', token)
		//         .end((err, res) => {
		//             expect(200)
		//             // expect(res.body.length).to.eq(0)
		//             // res.body.should.have.property('message').eql('Book updated!');
		//             // res.body.book.should.have.property('year').eql(1950);
		//             done();
		//         });
		// });
	});
	// })

	afterEach(() => {
		mockgoose.helper.reset();
		// .then(() => done())
		// .catch(err => done())
	});

	after((done) => {
		conn
			.close()
			.then(() => done())
			.catch((err) => done());
	});
});
