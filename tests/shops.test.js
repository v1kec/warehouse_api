let { nodeEnv } = require("../config/config");
process.env.NODE_ENV = "test";

const expect = require("chai").expect;
const should = require("chai").should;
const request = require("supertest");
const router = require("../routes/api/tagged");
const conn = require("../index.js");
const app = require("../server");
const token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlODMzOGE4NDdhZTJlMzIwMzMwYWIwMSIsImlhdCI6MTU5MTI3NTYyMSwiZXhwIjoxNTkxMjc5MjIxfQ.oGuf2e77EMsY8V9LAXvYpgkiZkbF-pyYpO_QN88sHg4";
const Shop = require("../models/shops");
const mongoose = require("mongoose");
const Mockgoose = require("mockgoose").Mockgoose;
const mockgoose = new Mockgoose(mongoose);

describe("Shops API", () => {
  before((done) => {
    conn
      .connect()
      .then(() => document())
      .catch((err) => done());
  });

  describe("GET Routes", () => {
    it("It should GET all shops with TOKEN", (done) => {
      request(app)
        .get("/api/shops")
        .set("x-auth-token", token)
        .end((err, res) => {
          expect("Content-Type", "/json/");
          expect(res.body).to.be.an("array");
          expect(res.body.length).to.equal(0);
          expect(200);
          done();
        });
    });

    it("It should NOT GET any shops due to wrong route", (done) => {
      request(app)
        .get("/api/shopps")
        .set("x-auth-token", token)
        .end((err, res) => {
          // Ne e specificiran statutos vo kontrolerot
          expect(404);
          done();
        });
    });

    it("It should GET all shops (1) with TOKEN", (done) => {
      let shop = new Shop({ name: "Testing_Shop" });
      shop.save((err, shop) => {
        request(app)
          .get("/api/shops")
          .set("x-auth-token", token)
          .end((err, res) => {
            expect("Content-Type", "/json/");
            expect(res.body).to.be.an("array");
            expect(res.body.length).to.equal(1);
            expect(200);
            done();
          });
      });
    });

    it("It should fail to GET all shops due to missing TOKEN", (done) => {
      request(app)
        .get("/api/item")
        .set("x-auth-token", "")
        .end((err, res) => {
          // Ne e specificiran statutos vo kontrolerot
          expect(404);
          done();
        });
    });
  });

  afterEach(() => {
    mockgoose.helper.reset();
    // .then(() => done())
    // .catch(err => done())
  });

  after((done) => {
    conn
      .close()
      .then(() => done())
      .catch((err) => done());
  });
});
