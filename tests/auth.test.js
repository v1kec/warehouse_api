let { nodeEnv } = require("../config/config");
process.env.NODE_ENV = "test";

const expect = require("chai").expect;
const should = require("chai").should;
const request = require("supertest");
const router = require("../routes/api/tagged");
const conn = require("../index.js");
const app = require("../server");
const token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlODMzOGE4NDdhZTJlMzIwMzMwYWIwMSIsImlhdCI6MTU5MTI3NTYyMSwiZXhwIjoxNTkxMjc5MjIxfQ.oGuf2e77EMsY8V9LAXvYpgkiZkbF-pyYpO_QN88sHg4";
const Tagged = require("../models/tagged");
const mongoose = require("mongoose");
const Mockgoose = require("mockgoose").Mockgoose;
const mockgoose = new Mockgoose(mongoose);

describe("Auth API", () => {
  before((done) => {
    conn
      .connect()
      .then(() => done())
      .catch((err) => done());
  });

  describe("GET user", () => {
    it("It should get existing user", (done) => {
      request(app)
        .get("/api/auth")
        .set()
        .end((err, res) => {
          expect();
        });
    });
  });
});
