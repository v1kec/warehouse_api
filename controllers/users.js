const bcrypt = require("bcryptjs");
const config = require("config");
const jwt = require("jsonwebtoken");
const User = require("../models/user");

module.exports = {
	createUser: async (req, res, next) => {
		const { firstName, lastName, email, password } = req.body;

		// Simple validation
		if (!firstName || !lastName || !email || !password)
			return res.status(400).json({ msg: "Please enter all fields" });

		// Check for existing user
		await User.findOne({ email }).then((user) => {
			if (user) return res.status(409).json({ msg: "User already exists" });

			const newUser = new User({ firstName, lastName, email, password });

			// Salt & Hash
			bcrypt.genSalt(10, (err, salt) => {
				bcrypt.hash(newUser.password, salt, (err, hash) => {
					if (err) throw err;
					newUser.password = hash;
					newUser.save().then((user) => {
						jwt.sign(
							{ id: user.id },
							config.get("jwtSecret"),
							{ expiresIn: 3600 },
							(err, token) => {
								if (err) throw err;
								res.json({
									token,
									user: {
										id: user.id,
										firstName: user.firstName,
										lastName: user.lastName,
										email: user.email,
									},
								});
							}
						);
					});
				});
			});
		});
	},
};
