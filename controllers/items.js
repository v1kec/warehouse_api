const Item = require("../models/items");

module.exports = {
	getItems: async (req, res, next) => {
		try {
			await Item.find().then((items) => res.json(items));
		} catch (err) {
			next(err);
		}
	},

	getSingleItem: async (req, res, next) => {
		try {
			await Item.findById(req.params.id).then((item) => res.json(item));
		} catch (err) {
			next(err);
		}
	},

	addItem: async (req, res, next) => {
		try {
			const newItem = new Item({
				name: req.body.name,
				manufacturer: req.body.manufacturer,
				quantity: req.body.quantity, // NEW
			});
			await newItem.save().then((item) => res.json(item));
		} catch (err) {
			next(err);
		}
	},

	editItem: async (req, res, next) => {
		try {
			await Item.findByIdAndUpdate(req.body.id, {
				$set: {
					name: req.body.name,
					manufacturer: req.body.manufacturer,
				},
			})
				// }, (res, err) => err ? console.log(err) : console.log(res))
				.then((item) => res.json(item));
		} catch (err) {
			next(err);
		}
	},

	deleteItem: async (req, res, next) => {
		try {
			await Item.findByIdAndRemove(req.params.id, (res, err) =>
				err ? console.log(err) : console.log(res)
			).then((items) => res.json(items));
		} catch (err) {
			next(err);
		}
	},
};
