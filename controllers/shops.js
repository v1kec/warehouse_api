const Shop = require("../models/shops");

module.exports = {
	// GET -> api/shops
	getShops: async (req, res, next) => {
		try {
			await Shop.find().then((shops) => res.json(shops));
		} catch (err) {
			next(err);
		}
	},

	getSingleShop: async (req, res, next) => {
		try {
			await Shop.findById(req.params.id).then((shop) => res.json(shop));
		} catch (err) {
			next(err);
		}
	},
	// POST -> api/shops
	addShop: async (req, res, next) => {
		const newShop = new Shop({
			name: req.body.name,
		});
		await newShop.save().then((shop) => res.json(shop));
	},
	// DELETE -> api/shops/{id}
	deleteShop: async (req, res, next) => {
		try {
			await Shop.findByIdAndRemove(req.params.id, (res, err) =>
				err ? console.log(err) : console.log(res)
			).then((shops) => res.json(shops));
		} catch (err) {
			next(err);
		}
	},
	// PUT -> api/shops/{id}
	editShop: async (req, res, next) => {
		try {
			await Shop.findByIdAndUpdate(req.params.id, {
				$set: {
					name: req.body.name,
				},
			}).then((shop) => res.json(shop));
		} catch (err) {
			next(err);
		}
	},
};
