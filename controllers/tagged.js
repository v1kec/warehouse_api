const Tagged = require("../models/tagged");

module.exports = {
	getTagged: async (req, res, next) => {
		try {
			await Tagged.find()
				.populate({ path: "shop" })
				.populate({ path: "items" })
				.then((tagged) => res.status(200).json(tagged));
		} catch (err) {
			next(err);
		}
	},

	getSingleTag: async (req, res, next) => {
		try {
			await Tagged.findOne({ shop: { _id: req.params.id } })
				.populate({ path: "shop" })
				.populate({ path: "items" })
				.then((tagged) => res.status(200).json(tagged));
		} catch (err) {
			next(err);
		}
	},

	deleteTag: async (req, res, next) => {
		try {
			await Tagged.findByIdAndRemove(req.params.id, (res, err) =>
				err ? console.log(err) : console.log(res)
			).then((tagged) => res.status(200).json(tagged));
			// if nothing is returned it should be status 204
		} catch (err) {
			next(err);
		}
	},

	addEditTagged: async (req, res, next) => {
		const existingShop = await Tagged.findOne({ shop: { _id: req.body.shop } });

		if (existingShop) {
			await Tagged.findOneAndUpdate(
				{ shop: { _id: req.body.shop } },
				{ $addToSet: { items: req.body.item } },
				{ returnOriginal: false },
				(err, doc) => doc.save()
			)
				.populate({ path: "items" })
				.then((updated) => res.json(updated))
				.catch((err) => console.log(err));
		} else {
			const newTag = new Tagged({
				shop: req.body.shop,
				items: req.body.item,
			});
			await newTag.save().then((tag) => res.json(tag));
		}
	},
};
