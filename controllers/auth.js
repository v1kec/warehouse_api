const User = require("../models/user");
const express = require("express");
const bcrypt = require("bcryptjs");
const config = require("config");
const jwt = require("jsonwebtoken");

module.exports = {
  authUser: async (req, res, next) => {
    const { email, password } = req.body;
    // Simple validation
    if (!email || !password) {
      return res.status(400).json({ msg: "Please enter all fields" });
    }
    // Check for existing user
    await User.findOne({ email }).then((user) => {
      if (!user) return res.status(400).json({ msg: "User does not exists" });

      // Password validation
      bcrypt.compare(password, user.password).then((isMatch) => {
        if (!isMatch)
          return res.status(400).json({ msg: "Invalid credentials" });

        jwt.sign(
          { id: user.id },
          config.get("jwtSecret"),
          { expiresIn: 3600 },
          (err, token) => {
            if (err) throw err;
            res.json({
              token,
              user: {
                id: user.id,
                user: user.name,
                email: user.email,
              },
            });
          }
        );
      });
    });
  },

  getUser: async (req, res, next) => {
    await User.findById(req.user.id)
      .select("-password")
      .then((user) => res.json(user));
  },
};
