module.exports = {
	env: {
		browser: true,
		commonjs: true,
		es2020: true,
	},
	extends: ["eslint:recommended", "plugin:react/recommended"],
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
		},
		end: {
			es6: true,
		},
		ecmaVersion: 11,
	},
	plugins: ["react"],
	rules: {},
};
