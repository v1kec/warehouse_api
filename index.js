const dotenv = require('dotenv')
dotenv.config()
const mongoose = require('mongoose');
const config = require('config')
const db = config.get('mongoURI');
// const { nodeEnv } = require('./config/config')
const { MongoMemoryServer } = require('mongodb-memory-server-global')
let mongoServer;


const connect = () => {
  return new Promise((resolve, reject) => {
    if (process.env.NODE_ENV === 'test') {
        const Mockgoose = require('mockgoose').Mockgoose;
        const mockgoose = new Mockgoose(mongoose);
        mockgoose.helper.setDbVersion('3.2.1')
                
        mockgoose.prepareStorage()
            .then(res => {
            mongoose.connect(db,
                { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true, useFindAndModify: false })
                .then((res, err) => {
                    console.log('Test MongoDB Connected...')
                    if (err) return reject(err);
                    resolve();
                })
            }).catch(reject);
        } else {
            mongoose.connect(db,
            { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true, useFindAndModify: false })
            .then((res, err) => {
                console.log('MongoDB Connected...')
                if (err) return reject(err);
                resolve();
            })
        }
  });
}

const close = () => {
  return mongoose.disconnect();
}

module.exports = { connect, close };
